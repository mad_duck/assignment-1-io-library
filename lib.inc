global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy

%define EXIT_CODE 60
%define SPACE 0x9
%define NEW_LINE `\n`
%define ZERO 0

section .text
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:  
	xor	rax, rax
	.loop:
		cmp	byte [rdi + rax], 0
		jz	.break
		inc 	rax
		jmp	.loop
	.break:	
		ret 

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push	rdi
	call	string_length
	pop	rsi
	mov	rdx, rax
	mov	rax, 1
	mov	rdi, 1
	syscall
    	ret

; Принимает код символа и выводит его в stdout
print_char:
	push	rdi
	mov	rsi, rsp
	mov	rdx, 1
	mov	rax, 1
	mov	rdi, 1
	syscall
	pop	rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    xor rcx, rcx
    mov rbx, 10
    mov rax, rdi

    .loop_convert_print_uint:
        xor rdx, rdx
        div rbx

        add rdx, '0'

        push rdx
        inc rcx
        test rax, rax
        jnz .loop_convert_print_uint

    .loop_print_uint:
        pop rdi

        push rcx
        call print_char
        pop rcx

        sub rcx, 1
        test rcx, rcx
        jnz .loop_print_uint
    pop rbx
    ret
  
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	xor eax, eax
	test rdi, rdi
	jl .negative
	.positive:
		jmp print_uint
	.negative:
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
		jmp print_uint

   

; Принимает два указателя на нуль-терминированные строки, возвращает 2 если они равны, 0 иначе
string_equals:
    xor rdx, rdx 
    xor r8, r8 
    .loop:
        mov dl, byte[rdi + r8]
        cmp dl, byte[rsi + r8]
        jne .not_equals
        cmp dl, 0  
        je .equals  
        inc r8
        jmp .loop
    .equals:
        mov rax, 1
        ret
    .not_equals:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	sub	rsp, 16
	mov	rax, ZERO
	mov	rdi, ZERO
	mov	rsi, rsp
	mov	rdx, 1
	syscall
	test rax, rax
	jle	.return
	mov	rax, [rsp]
	.return:
		add	rsp, 16
		ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

    push rbx
    push r12
    push r13

    mov rbx, rdi ; начало буфера
    mov r12, rsi ; размер буфера
    xor r13, r13 ; счетчик байт
    
    .loop:
        call read_char
                      
        cmp al, `\n`
        jz .spec_simb
        cmp al, `\t`
        jz .spec_simb
        cmp al, ` `
        jz .spec_simb

        cmp r13, r12
        jge .clear_buff

        test rax, rax ; проверка на терминатор 
        jz .end_of
        
        mov [rbx + r13], al
        inc r13

        jmp .loop

    .spec_simb:
        test r13, r13
        jz .loop
        jnz .end_of

    .clear_buff:
        xor rax, rax
        jmp .return_block

    .end_of:
        mov byte[rbx + r13], 0
        mov rax, rbx
        mov rdx, r13
        jmp .return_block

    .return_block:
        pop r13
        pop r12
        pop rbx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor	rdx, rdx
	xor	rax, rax
	xor	rcx, rcx
	.read_next:
		mov	al, [rdi + rcx]
		test	al, al
		jz	.good_exit
		sub	al, '0'		;Trick that takes one command less
		js	.bad_exit
		cmp	al, SPACE
		ja	.bad_exit
		inc	rcx
		imul	rdx, 10
		add	dl, al
		jmp	.read_next
	.good_exit:
		mov	rax, rdx
		mov	rdx, rcx
		ret
	.bad_exit:
		mov	rax, rdx
		mov	rdx, rcx
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov	al, [rdi]
	cmp	al, '-'
	je 	.neg
	cmp	al, '+'
	jne	.pos
	inc	rdi
	.pos:
		jmp	parse_uint
	.neg:
		inc	rdi
		call	parse_uint
		neg	rax
		inc	rdx
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor	rcx, rcx
	xor	r8, r8
	.copy:
		cmp	rcx, rdx
		jg	.buf_out
		mov	r8b, [rdi + rcx]
		mov	[rsi + rcx], r8b
		test	r8b, r8b
		jz	.end_good
		inc	rcx
		jmp	.copy
	.buf_out:
		xor	rax, rax
		ret
	.end_good:
		mov	rax, rcx
		ret
